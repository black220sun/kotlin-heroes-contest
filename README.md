# Kotlin Heroes: Episode 2

### Problem A. Three problems
https://codeforces.com/contest/1211/problem/A

### Problem B. Traveling Around the Golden Ring of Berland
https://codeforces.com/contest/1211/problem/B

### Problem C. Ice Cream
https://codeforces.com/contest/1211/problem/C

### Problem D. Teams
https://codeforces.com/contest/1211/problem/D

### Problem E. Double Permutation Inc.
https://codeforces.com/contest/1211/problem/E

### Problem F. kotlinkotlinkotlinkotlin...
https://codeforces.com/contest/1211/problem/F

### Problem G. King's Path
https://codeforces.com/contest/1211/problem/G

### Problem H. Road Repair in Treeland
https://codeforces.com/contest/1211/problem/H

### Problem I. Unusual Graph
https://codeforces.com/contest/1211/problem/I

### Standings
https://codeforces.com/contest/1211/standings
