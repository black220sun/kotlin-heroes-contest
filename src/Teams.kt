fun main() {
    val (_, a, b, k) = readLine()!!.split(" ").map { str -> str.toInt() }
    val r = readLine()!!
        .split(" ")
        .map { str -> str.toInt() }
        .groupingBy { it }
        .eachCount()
        .toMutableMap()
    var res = 0L
    for (key in r.keys) {
        var first = r.getValue(key)
        var second = r.getOrDefault(key * k, 0)
        while (first >= a && second >= b) {
            res += 1
            first -= a
            second -= b
            r[key] = first
            r[key * k] = second
        }
    }
    println(res)
}