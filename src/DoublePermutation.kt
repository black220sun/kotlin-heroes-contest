fun main() {
    readLine()
    val ints = readLine()!!.split(" ").map { it.toInt() }
    val counts = ints.groupingBy { it }.eachCount().toMutableMap()
    val builder = StringBuilder()
    var lastMatch = -1
    for ((index, i) in ints.withIndex()) {
        val count = counts.getValue(i)
        if (count !in listOf(-1, 2)) {
            builder.append('B')
            continue
        }
        val first = ints.indexOfFirst { it == i }
        val last = ints.indexOfLast { it == i }
        if (index == first && last > first && last > lastMatch) {
            builder.append('R')
            lastMatch = last
            counts[i] = -1
        } else if (count == -1){
            builder.append('G')
        } else {
            builder.append('B')
        }
    }
    println(builder)
}
