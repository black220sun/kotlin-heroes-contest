fun main() {
    readLine()
    val inputs = readLine()!!.split(" ").map { str -> str.toInt() }
    var max = inputs[0]
    var maxIndex = 0
    inputs.forEachIndexed { index, value ->
        if (value >= max) {
            max = value
            maxIndex = index
        }
    }
    --max
    println(max.toLong() * inputs.size + maxIndex + 1)
}