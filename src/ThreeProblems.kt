fun main() {
    readLine()
    val inputs = readLine()!!.split(" ").mapIndexed { i, str -> i to str.toInt() }
    val result = inputs.asSequence()
        .distinctBy { pair -> pair.second }
        .sortedBy { pair -> pair.second }
        .take(3)
        .map { pair -> pair.first + 1 }
        .toList()
    println(if (result.size == 3) result.joinToString(separator = " ") else "-1 -1 -1")

}