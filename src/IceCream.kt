fun main() {
    var (n, k) = readLine()!!.split(" ").map { str -> str.toLong() }
    val days = ArrayList<List<Long>>()
    while (n-- > 0) {
        days.add(readLine()!!.split(" ").map { str -> str.toLong() })
    }
    if (days.map { nums -> nums[0]}.sum() > k) {
        println(-1)
        return
    }
    if (days.map { nums -> nums[1]}.sum() < k) {
        println(-1)
        return
    }
    var cost = 0L
    for (nums in days) {
        k -= nums[0]
        cost += nums[0] * nums[2]
    }
    while (k > 0) {
        for (nums in days.sortedBy { nums -> nums[2] }) {
            var consume = nums[1] - nums[0]
            if (consume > k) consume = k
            k -= consume
            cost += consume * nums[2]
            if (k <= 0)
                break
        }
    }
    println(cost)
}