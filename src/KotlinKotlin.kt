fun main() {
    val n = readLine()!!.toInt()
    val lines = 1.rangeTo(n).map { readLine() }.toMutableList()
    val result = ArrayList<Int>()
    val value = "kotlin"
    var index = 0
    for (i in 1.rangeTo(n)) {
        val find = lines.indexOfFirst { it!!.startsWith(value[index]) }
        val string = lines[find]!!
        lines[find] = ""
        index = (value.indexOf(string[string.length - 1]) + 1) % value.length
        result.add(find + 1)
    }
    println(result.joinToString(separator = " "))
}